(defun org-pdcite-parse-complete-test (str parser &optional val)
  (with-temp-buffer
    (insert str)
    (goto-char (point-min))
    (if val
        (should (equal val (funcall parser)))
      (should (not (eq nil (funcall parser)))))
    (should (eq (point) (point-max))))
  ;; with leading whitespace
  (unless (string-match "^ " str)
    (org-pdcite-parse-complete-test
     (format " %s" str) parser val)))

(defun org-pdcite-parse-fail-test (str parser)
  (with-temp-buffer
    (insert str)
    (goto-char (point-min))
    (should (eq nil (funcall parser)))
    (should (eq (point) (point-min)))))

(ert-deftest org-pdcite-skip-whitespace-test ()
  (with-temp-buffer
    (insert " aa ")
    (goto-char (point-min))
    (org-pdcite-skip-whitespace)
    (should (eq (point) 2))))

(ert-deftest org-pdcite-parse-char-test ()
  (let ((parser (org-pdcite-make-char-parser ?a)))
    (org-pdcite-parse-complete-test "a" parser t)
    (org-pdcite-parse-fail-test "b" parser)))

(ert-deftest org-pdcite-parse-alt-test ()
  (let ((parser (org-pdcite-make-alt-parser
                 (org-pdcite-make-char-parser ?a)
                 (org-pdcite-make-char-parser ?b))))
    (org-pdcite-parse-complete-test "a" parser t)
    (org-pdcite-parse-complete-test "b" parser t)
    (org-pdcite-parse-fail-test "c" parser)))

(ert-deftest org-pdcite-parse-seq-test ()
  (let ((parser (org-pdcite-make-seq-parser
                 (org-pdcite-make-char-parser ?a)
                 (org-pdcite-make-char-parser ?b))))
    (org-pdcite-parse-complete-test "ab" parser t)
    (org-pdcite-parse-fail-test "ba" parser)
    (org-pdcite-parse-fail-test "a" parser)
    (org-pdcite-parse-fail-test "b" parser)))

(ert-deftest org-pdcite-parse-regexp-test ()
  (let ((parser (org-pdcite-make-regexp-parser
                 "ab\\(c?\\)"
                 (lambda ()
                   `(:c ,(string= "c" (match-string 1)))))))
    (org-pdcite-parse-fail-test "a" parser)
    (org-pdcite-parse-complete-test "ab" parser '(:c nil))
    (org-pdcite-parse-complete-test "abc" parser '(:c t))))

(ert-deftest org-pdcite-parse-optional-test ()
  (let ((parser1 (org-pdcite-make-opt-parser
                  (org-pdcite-make-char-parser ?a)
                  (lambda (val)
                    `(:wrapped ,val))))
        (parser2 (org-pdcite-make-opt-parser
                  (org-pdcite-make-regexp-parser
                   "ab\\(c?\\)"
                   (lambda ()
                     `(:c ,(string= "c" (match-string 1))))))))
    (org-pdcite-parse-complete-test "a" parser1 '(:wrapped t))
    (with-temp-buffer
      (insert "b")
      (goto-char (point-min))
      (should (eq t (funcall parser1)))
      (should (eq (point) (point-min))))
    (org-pdcite-parse-complete-test "abc" parser2 '(:c t))
    (with-temp-buffer
      (insert "ad")
      (goto-char (point-min))
      (should (eq t (funcall parser2)))
      (should (eq (point) (point-min))))))

(ert-deftest org-pdcite-parse-one-or-more-test ()
  (let ((parser (org-pdcite-make-one-or-more-parser
                  (org-pdcite-make-char-parser ?a))))
    (org-pdcite-parse-fail-test "" parser)
    (org-pdcite-parse-complete-test "a" parser)
    (org-pdcite-parse-complete-test "aaaaaaaaaaaaaaaaaaaa" parser)
    (org-pdcite-parse-fail-test "ba" parser)))

(ert-deftest org-pdcite-parse-one-or-more-wrapper-test ()
  (let ((parser (org-pdcite-make-one-or-more-parser
                 (org-pdcite-make-char-parser ?a)
                 (lambda (v)
                   `(:list ,v)))))
    (org-pdcite-parse-complete-test "a" parser '(:list (t)))))

(ert-deftest org-pdcite-parse-zero-or-more-test ()
  (let ((parser (org-pdcite-make-zero-or-more-parser
                  (org-pdcite-make-char-parser ?a))))
    (org-pdcite-parse-complete-test "" parser t)
    (org-pdcite-parse-complete-test "a" parser '(t))
    (org-pdcite-parse-complete-test "aaaaaaaaaaaaaaaaaaaa" parser)
    (with-temp-buffer
      (insert "ba")
      (goto-char (point-min))
      (should (eq t (funcall parser)))
      (should (eq (point-min) (point))))))

(ert-deftest org-pdcite-parse-zero-or-more-wrapper-test ()
  (let ((parser (org-pdcite-make-zero-or-more-parser
                 (org-pdcite-make-char-parser ?a)
                 (lambda (v)
                   `(:list ,v)))))
    (org-pdcite-parse-complete-test "" parser '(:list t))
    (org-pdcite-parse-complete-test "a" parser '(:list (t)))))

(ert-deftest org-pdcite-parse-short-cite-test ()
  (org-pdcite-parse-complete-test
   "@foo" #'org-pdcite-short-cite-parser
   '(:suppress-author nil :citekey "foo"))
  (org-pdcite-parse-complete-test
   "@foo:2001" #'org-pdcite-short-cite-parser
   '(:suppress-author nil :citekey "foo:2001"))
  (org-pdcite-parse-complete-test
   "-@foo:2001" #'org-pdcite-short-cite-parser
   '(:suppress-author t :citekey "foo:2001")))

(ert-deftest org-pdcite-parse-greedytoken-test ()
  (org-pdcite-parse-complete-test
   "hello" #'org-pdcite-greedy-token-parser "hello")
  (with-temp-buffer
    (insert "world;")
    (goto-char (point-min))
    (should (string= "world" (org-pdcite-greedy-token-parser)))
    (should (eq t (looking-at ";")))))

(ert-deftest org-pdcite-parse-suffix-test ()
  (org-pdcite-parse-complete-test
   "hello" #'org-pdcite-suffix-parser '(:suffix "hello"))
  (org-pdcite-parse-complete-test
   "hello world" #'org-pdcite-suffix-parser '(:suffix "hello" "world")))

(ert-deftest org-pdcite-wrap-parser-test ()
  (let ((parser (org-pdcite-wrap-parser
                 (org-pdcite-make-char-parser ?a)
                 (lambda (val)
                   `(:foo ,val)))))
    (org-pdcite-parse-complete-test
     "a" parser '(:foo t))))

(ert-deftest org-pdcite-parse-word-with-digits-test ()
  (org-pdcite-parse-complete-test
   "10" #'org-pdcite-word-with-digits-parser)
  (org-pdcite-parse-complete-test
   "xv" #'org-pdcite-word-with-digits-parser)
  (org-pdcite-parse-fail-test
   "foobar" #'org-pdcite-word-with-digits-parser)
  (org-pdcite-parse-fail-test
   "chap." #'org-pdcite-word-with-digits-parser)
  (org-pdcite-parse-complete-test
   "foo1128bar" #'org-pdcite-word-with-digits-parser))

(ert-deftest org-pdcite-parse-locator-word-test ()
  (org-pdcite-parse-complete-test
   "page" #'org-pdcite-locator-word-parser)
  (org-pdcite-parse-complete-test
   "pages" #'org-pdcite-locator-word-parser)
  (org-pdcite-parse-complete-test
   "chap." #'org-pdcite-locator-word-parser))

(ert-deftest org-pdcite-parse-locator-test ()
  (org-pdcite-parse-complete-test
   "10" #'org-pdcite-locator-parser)
  (org-pdcite-parse-complete-test
   ", p. 10" #'org-pdcite-locator-parser)
  (org-pdcite-parse-complete-test
   "§ 10" #'org-pdcite-locator-parser)
  (org-pdcite-parse-complete-test
   "chap. 10" #'org-pdcite-locator-parser)
  (org-pdcite-parse-complete-test
   "chapter xv" #'org-pdcite-locator-parser)
  (org-pdcite-parse-fail-test
   ", hello" #'org-pdcite-locator-parser)
  (org-pdcite-parse-fail-test
   "hello 10" #'org-pdcite-locator-parser))

(ert-deftest org-pdcite-parse-bracketed-cite-test ()
 (org-pdcite-parse-complete-test
   "[see @item1 p. 34-35]" #'org-pdcite-bracketed-cite-parser)
  (org-pdcite-parse-complete-test
   "[@item1 pp. 33, 35-37, and nowhere else]" #'org-pdcite-bracketed-cite-parser)
  (org-pdcite-parse-complete-test
   "[@item1 and nowhere else]" #'org-pdcite-bracketed-cite-parser)
  (org-pdcite-parse-complete-test
   "[@item3]" #'org-pdcite-bracketed-cite-parser)
  (org-pdcite-parse-complete-test
   "[see @item2 chap. 3; @item3; @item1]" #'org-pdcite-bracketed-cite-parser)
  (org-pdcite-parse-complete-test
   "[-@item12, p. 44]" #'org-pdcite-bracketed-cite-parser))

(ert-deftest org-pdcite-parse-full-cite-test ()
  (org-pdcite-parse-complete-test
   "@doe:2015" #'org-pdcite-full-cite-parser)
  (org-pdcite-parse-complete-test
   "cf @doe:2015" #'org-pdcite-full-cite-parser
   '(:prefix "cf" :suppress-author nil :citekey "doe:2015"))
  (org-pdcite-parse-complete-test
   "see also @doe:2015, p. 10" #'org-pdcite-full-cite-parser
   '(:prefix "see also" :suppress-author nil :citekey "doe:2015" :locator-word "p." :locator "10"))
  (org-pdcite-parse-complete-test
   "see also @doe:2015, p. 10 and others" #'org-pdcite-full-cite-parser))

(ert-deftest org-pdcite-parse-top-cite-test ()
  (org-pdcite-parse-complete-test
   "@item1" #'org-pdcite-top-cite-parser)
  (org-pdcite-parse-complete-test
   "@item1 [p. 30]" #'org-pdcite-top-cite-parser)
  (org-pdcite-parse-complete-test
   "@item1 [p. 30, with suffix]" #'org-pdcite-top-cite-parser)
  (org-pdcite-parse-complete-test
   "@item1 [-@item2 p. 30; see also @item3]" #'org-pdcite-top-cite-parser)
  (org-pdcite-parse-complete-test
   "[see @item1 p. 34-35; also @item3 chap. 3]" #'org-pdcite-top-cite-parser)
  (org-pdcite-parse-complete-test
   "[see @item1 p. 34-35]" #'org-pdcite-top-cite-parser)
  (org-pdcite-parse-complete-test
   "[@item1 pp. 33, 35-37, and nowhere else]" #'org-pdcite-top-cite-parser)
  (org-pdcite-parse-complete-test
   "[@item1 and nowhere else]" #'org-pdcite-top-cite-parser)
  (org-pdcite-parse-complete-test
   "[@item3]" #'org-pdcite-top-cite-parser)
  (org-pdcite-parse-complete-test
   "[see @item2 chap. 3; @item3; @item1]" #'org-pdcite-top-cite-parser)
  (org-pdcite-parse-complete-test
   "[-@item12, p. 44]" #'org-pdcite-top-cite-parser))
